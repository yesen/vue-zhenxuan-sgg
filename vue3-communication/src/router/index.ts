import { createRouter, createWebHashHistory } from "vue-router";
import Home from "../views/Home.vue";
import PropsTest from "../views/01_props/PropsTest.vue";
import EventTest from "../views/02_custom_event/EventTest.vue";
import EventBusTest from "../views/03_event_bus/EventBusTest.vue";
import VModelTest from "../views/04_v-model/VModelTest.vue";
import AttrsListenersTest from "../views/05_attrs_listeners/AttrsListenersTest.vue";
import RefParent from "../views/06_ref_parent/RefParent.vue";
import ProvideInject from "../views/07_provide_inject/ProvideInject.vue";
import Pinia from "../views/08_pinia/index.vue";
import Slot from "../views/09_slot/Slot.vue";

export default createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: "/",
      component: Home,
    },
    {
      path: "/props",
      component: PropsTest,
    },
    {
      path: "/event",
      component: EventTest,
    },
    {
      path: "/event-bus",
      component: EventBusTest,
    },
    {
      path: "/v-model",
      component: VModelTest,
    },
    {
      path: "/attrs",
      component: AttrsListenersTest,
    },
    {
      path: "/ref-parent",
      component: RefParent,
    },
    {
      path: "/provide-inject",
      component: ProvideInject,
    },
    {
      path: "/pinia",
      component: Pinia,
    },
    {
      path: "/slot",
      component: Slot,
    },
  ],
});
