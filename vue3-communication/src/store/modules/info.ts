import { defineStore } from "pinia";

const useInfoStore = defineStore("info", {
  state() {
    return {
      count: 99,
    };
  },
  actions: {
    incrementCount() {
      this.$state.count++;
    },
  },
  getters: {},
});

export default useInfoStore;
