import { defineStore } from "pinia";
import { ref, computed } from "vue";

export type TodoItem = { id: number; title: string; done: boolean };

const useTodoStore = defineStore("todo", () => {
  const todos = ref<TodoItem[]>([
    { id: 1, title: "吃饭", done: false },
    { id: 2, title: "睡觉", done: true },
    { id: 3, title: "打豆豆", done: false },
  ]);

  const add = (item: TodoItem) => {
    todos.value.unshift(item);
  };

  const total = computed<number>(() => todos.value.length);
  const doneTodos = computed<TodoItem[]>(() =>
    todos.value.filter((t) => t.done)
  );
  const doneTotal = computed<number>(() => doneTodos.value.length);
  return { todos, add, total, doneTodos, doneTotal };
});

export default useTodoStore;
